# O tej mapie
Mapa została zrobiona w listopadzie 2016 roku i wyglądała ona zupełnie inaczej. Niebo było inne, a platformy miały jednolity kolor - czerwona dla czerwonej drużyny, niebieska - dla niebieskiej.

## Tak wyglądała ta mapa na początku
![](./201611xx/1.png)
![](./201611xx/2.png)
![](./201611xx/3.png)
![](./201611xx/4.png)
![](./201611xx/5.png)
![](./201611xx/6.png)
![](./201611xx/7.png)

## Potem...
We wrześniu 2018 roku powiększyłem platformy, bo były za małe. Potem zmieniłem odcienie kolorów niektórych elementów, żeby nie zlewało się wszystko ze sobą.
![](./20180916/1.png)

Jeszcze potem zmieniłem tekstury, żeby ładniej to wyglądało i dodałem "herby" oznaczające drużynę, do której należy platforma.
![](./20180916/2.png)

Ostatecznie musiałem poszerzyć wyjścia z miejsc, w których pojawiają się gracze, bo jeden ludek mógł je zablokować.
![](./20180916/3.png)
![](./20180916/4.png)
![](./20180916/5.png)

Byłem świadkiem zdarzenia, w którym aż trzech graczy blokowało przejście.
![](./20180916/6.png)

## Tak wyglądało przejście przed jego poszerzeniem
![](./update20190101/1a.png)
![](./update20190101/2a.png)
![](./update20190101/3a.png)

## A tak teraz wygląda
![](./update20190101/1b.png)
![](./update20190101/2b.png)
![](./update20190101/3b.png)
