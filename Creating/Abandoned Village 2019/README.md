# Lista zmian przed pierwszym wydaniem mapy
## 2019-06-13
Powiększono i prawie skończono dom z garażem, teksturami zajmę się później
![](./20190613/1.png)

## 2019-06-14
Dom zrobiony i powiększony, oświetlenie dodane. Jutro zostaną zmienione tekstury
![](./20190614/1.png)
![](./20190614/2.png)
![](./20190614/3.png)
![](./20190614/4.png)
![](./20190614/5.png)

## 2019-06-15
Oteksturowano cały dom i zrobiono przesuwne drzwi garażowe. Jutro w nim wszystkie drzwi będą przesuwne i zostaną dodane meble. No i też zajmę się windą
![](./20190615/1.png)

## 2019-06-16
Drzwi przesuwane zrobione, winda ogarnięta, udało mi się umeblować jedynie kuchnię połączoną z jadalnią. Jutro umebluję resztę w tym domu resztę pomieszczeń i jak skończę to go zdubluję i umieszczę go na drugim końcu mapy
![](./20190616/1.png)

## 2019-06-18
Umeblowano domy z garażem, powielono go i umieszczono go w innym miejscu. Specjalnie zostawiłem jedno pomieszczenie puste.
![](./20190618/1.png)
![](./20190618/2.png)
![](./20190618/3.png)
![](./20190618/4.png)

## 2019-06-20
Oto co udało mi się wczoraj zrobić/ porozstawiałem słupy wszędzie tam, gdzie chciałem, żeby były i zniekształciłem drogę polną
![](./20190619/1.png)
![](./20190619/2.png)

Dodano pole z traktorem, dwa domy, zniekształcono drogę tuż przy bloku, rozstawiono armory spoty i kamery startowe. Miałem dzisiaj porozstawiać spawnpointy graczy, ale po pierwsze jest późno, a po drugie zbliża się burza, więc zrobię to jutro.
![](./20190620/1.png)
![](./20190620/2.png)
![](./20190620/3.png)
![](./20190620/4.png)
![](./20190620/5.png)
![](./20190620/6.png)

## 2019-06-21
Porozstawiano spawnpointy, dodano ogrodzenie dookoła wieży, dodano piętro w domu po stronie czerwonych, dodano płot i rozwalony dom. Jutro zrobię przesuwane bramy przy wieży.
![](./20190621/1.png)
![](./20190621/2.png)

## 2019-06-22
Dodano drzewa, zrobiono przesuwającą się bramę do wieży, dodano prowizoryczny (póki co) skład broni, zniekształcono nową drogę polną i dodano niespodziankę na tej mapie (będzie można ją zobaczyć dopiero wtedy jak dodam tą mapę)
![](./20190622/1.png)
![](./20190622/2.png)
![](./20190622/3.png)
![](./20190622/4.png)
![](./20190622/5.png)

## 2019-06-24
Dodano latarnie na słupach, parę domów, puste półki w składzie broni, które będą wypełnione broniami z tej gry (o ile Max się zgodzi), boisko do gry w nogę i zaczęto robić las.
![](./20190624/1.png)
![](./20190624/2.png)
![](./20190624/3.png)
![](./20190624/4.png)
![](./20190624/5.png)
![](./20190624/6.png)

## 2019-06-28
Zrobiono las (do zrobienia drugi), zmieniono nieco uształtowanie terenu, dodano latarnię na jednym słupie, kilka domów, ogrodzenia do oznaczenia działek (kilka zostawię pustych)
![](./20190628/1.png)
![](./20190628/2.png)
![](./20190628/3.png)
